# Irregs_final

## Name
Online Quiz - Final Project

## Description
A final project for the course Web Technologies.

## Project status
Tasks: 

    Teacher module (NODE.JS)
    -View Results (STATISTICS)
    -Enhance the results viewing features of your previous work so that more options are provided to the user.
    -The admin must be bale to create an exam, post an exam, and stop it from being available.
    -Create an exam (Final edits).
    -CSS

    Student/Exam Module (PHP)
    -Retrieve exam (GET).
    -Upload exam answers (POST).
    -The team must determine when and how the student can view the results of an exam taken.
    -Session handling must be integrated in the exam module.

    Overall task
    -*HOSTING*