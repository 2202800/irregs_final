-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 03, 2023 at 07:59 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `irregs`
--

-- --------------------------------------------------------

--
-- Table structure for table `checking_table`
--

CREATE TABLE `checking_table` (
  `question_id` int(11) NOT NULL,
  `correctAnswer` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `checking_table`
--

INSERT INTO `checking_table` (`question_id`, `correctAnswer`) VALUES
(1, '1976'),
(2, 'October 2021'),
(3, 'Microsoft'),
(4, 'Stockholm'),
(5, 'Reddit'),
(6, 'Twitter'),
(7, 'Amazon'),
(8, 'Google'),
(9, '1997'),
(10, 'Netflix'),
(121, 'LG'),
(122, 'Git'),
(123, 'Google'),
(124, 'Microsoft'),
(125, 'Spotify'),
(126, 'Trivago'),
(127, 'Facebook'),
(128, 'Netflix'),
(129, 'Twitter'),
(130, 'Reddit'),
(131, 'Tim Cook'),
(132, 'Satya Nadella'),
(133, 'Mark Zuckerberg'),
(134, 'Sundar Pichai'),
(135, 'Andy Jassy'),
(136, 'Ted Sarandos'),
(137, 'Susan Wojcicki'),
(138, 'Daniel Ek'),
(139, 'Steve Huffman'),
(140, 'Parag Agrawal'),
(141, 'asd'),
(142, 'C1 NEW'),
(143, 'C1'),
(144, 'C1'),
(145, 'True');

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE `choices` (
  `choice_id` int(11) NOT NULL,
  `choices` varchar(100) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`choice_id`, `choices`, `question_id`) VALUES
(62, 'Stockholm', 4),
(4, '1967', 1),
(3, '1976', 1),
(2, '2002', 1),
(1, '2021', 1),
(7, 'November 2020', 2),
(8, 'January 2021', 2),
(9, 'February 2020', 2),
(10, 'October 2021', 2),
(11, 'Apple', 3),
(12, 'Microsoft', 3),
(13, 'Bighard', 3),
(14, 'Cherry Mobile', 3),
(63, 'Jerusalem', 4),
(64, 'Baguio City', 4),
(65, 'Tokyo', 4),
(66, 'Meta', 5),
(67, 'Reddit', 5),
(68, 'Twitter', 5),
(69, 'Bilibili', 5),
(70, 'Telegram', 6),
(71, 'Meta', 6),
(72, 'Twitter', 6),
(73, 'Messenger', 6),
(74, 'Shopee', 7),
(75, 'Lazada', 7),
(76, 'Marketplace', 7),
(77, 'Amazon', 7),
(78, 'Google', 8),
(79, 'Amazon', 8),
(80, 'Reddit', 8),
(81, 'Netflix', 8),
(82, '1999', 9),
(83, '1997', 9),
(84, '2000', 9),
(85, '2020', 9),
(86, 'Youtube', 10),
(87, 'Netflix', 10),
(88, 'Bilibili', 10),
(89, 'KissAnime', 10),
(90, 'Tim Cook', 131),
(91, 'Satya Nadella', 132),
(92, 'Mark Zuckerberg', 133),
(93, 'Sundar Pichai', 134),
(94, 'Andy Jassy', 135),
(95, 'Ted Sarandos', 136),
(96, 'Susan Wojcicki', 137),
(97, 'Daniel Ek', 138),
(98, 'Steve Huffman', 139),
(99, 'Parag Agrawal', 140),
(107, 'C4 NEW', 142),
(106, 'C2 NEW', 142),
(105, 'C3 NEW', 142),
(104, 'C1 NEW', 142),
(108, 'C1', 143),
(109, 'C3', 143),
(110, 'C2', 143),
(111, 'C4', 143),
(112, 'C1', 144),
(113, 'C3', 144),
(114, 'C2', 144),
(115, 'C4', 144),
(116, 'True', 145),
(117, 'False', 145);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `question_type` varchar(25) NOT NULL,
  `points` int(100) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `quiz_id`, `question`, `question_type`, `points`) VALUES
(1, 1, 'Apple was founded on April 1,____?', 'Multiple Choice', 1),
(2, 1, 'When did Facebook change its name to Meta?', 'Multiple Choice', 1),
(3, 1, 'The companys name which was derived from the words microcomputer and software.', 'Multiple Choice', 1),
(4, 1, 'Spotify was founded in 2006 in ______?', 'Multiple Choice', 1),
(5, 1, 'This bulletin board-based system was created mainly to share hobbies and interests.', 'Multiple Choice', 1),
(6, 1, 'This platform was created to share short messages with a group of people, similar to sending text messages', 'Multiple Choice', 1),
(7, 1, 'This technological company’s purpose is mainly to simplify online transactions for consumers.', 'Multiple Choice', 1),
(8, 1, 'This was originally named “Backrub”.', 'Multiple Choice', 1),
(9, 1, 'Netflix was founded in ____ in a small California city called Scotts Valley in Santa Cruz county.', 'Multiple Choice', 1),
(10, 1, 'It was created when the founder was fined $40 by renting the movie Apollo-13 from long-dead Buster and returning it six weeks later after its due date.', 'Multiple Choice', 1),
(121, 2, 'Company\'s slogan is \"Life is Good\"', 'Fill In The Blanks', 1),
(122, 2, 'is a software that enables the user to track changes on files in order to improve development for a program', 'Fill In The Blanks', 1),
(123, 2, 's mission of this browser is to organize the information of the world and make it universally accessible and useful.', 'Fill In The Blanks', 1),
(124, 2, 'is a company known for its Operating System software and \'Office\' suite products.', 'Fill In The Blanks', 1),
(125, 2, 'focuses on music and has been a top competitor because of user experience and low pricing.', 'Fill In The Blanks', 1),
(126, 2, 'declares their company to be the world\'s biggest online hotel search site and they specialize in internet-related services and products in the hotel.', 'Fill In The Blanks', 1),
(127, 2, 'is the third most popular site in the world. It is the most used website after only Google and YouTube.', 'Fill In The Blanks', 1),
(128, 2, 'is a subscription-based streaming service which allows the subscribers to watch shows using the internet.', 'Fill In The Blanks', 1),
(129, 2, 'Company\'s mission is to give everyone the power to make and share ideas and information instantly without barriers.', 'Fill In The Blanks', 1),
(130, 2, 'Company\'s vision is to bring community and belonging to everyone for the entire world.', 'Fill In The Blanks', 1),
(131, 3, 'Apple', 'Matching Type', 1),
(132, 3, 'Microsoft', 'Matching Type', 1),
(133, 3, 'Meta', 'Matching Type', 1),
(134, 3, 'Google', 'Matching Type', 1),
(135, 3, 'Amazon', 'Matching Type', 1),
(136, 3, 'Netflix', 'Matching Type', 1),
(137, 3, 'YouTube', 'Matching Type', 1),
(138, 3, 'Spotify', 'Matching Type', 1),
(139, 3, 'Reddit', 'Matching Type', 1),
(140, 3, 'Twitter', 'Matching Type', 1),
(142, 128, 'Q1 NEW', 'Multiple Choice', 1),
(143, 129, 'Q1', 'Multiple Choice', 1),
(144, 130, 'Q1', 'Multiple Choice', 1),
(145, 130, 'T OR F?', 'True or False', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `quiz_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `status` varchar(30) NOT NULL,
  `is_visible` text NOT NULL,
  `author` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`quiz_id`, `title`, `status`, `is_visible`, `author`) VALUES
(1, 'Quiz 1 - Technology Quiz Multiple Choice', 'Accepting Responses', 'true', '2200103'),
(2, 'Quiz 2 - Technology Quiz Fill in the Blanks', 'Accepting Responses', 'false', '2200103'),
(3, 'Quiz 3 - Technology Quiz Matching type', 'Not Accepting Responses', 'false', '2200103');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(7) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `user_status` varchar(30) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `user_avail` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `first_name`, `last_name`, `password`, `user_status`, `user_type`, `user_avail`) VALUES
(2200100, 'admin', 'one', 'pass', 'offline', 'admin', 'true'),
(2200101, 'student', 'one', 'pass', 'offline', 'student', 'true'),
(2200102, 'student', 'two', 'pass', 'offline', 'student', 'true'),
(2200103, 'teacher', 'one', 'pass', 'offline', 'teacher', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `user_choices`
--

CREATE TABLE `user_choices` (
  `user_id` int(7) NOT NULL,
  `question_id` int(5) NOT NULL,
  `user_answer` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_scores`
--

CREATE TABLE `user_scores` (
  `ID` int(7) NOT NULL,
  `score` int(4) NOT NULL,
  `quiz_id` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD UNIQUE KEY `choice_id` (`choice_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`quiz_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `choices`
--
ALTER TABLE `choices`
  MODIFY `choice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `quiz_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
