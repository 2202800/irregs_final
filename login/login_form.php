<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name = "viewport" content="width = device-width, initial-scale = 1.0">
    <title>TEAM IRREGS | LOGIN </title>
    <link rel="stylesheet" href="../styles/style.css" type = "text/css">
</head>

<body>
<div class="center">
    <img id="irregs_logo_login" src="../lib/images/TEAM_IRREGS_LOGO.png" height="300px" width="500px" >
    <div class = "container">
        <form name="login-form" action = "login.php" method = "POST">

            <h2>USER LOGIN</h2>
            <?php echo $_SESSION['idno']?>

            <?php if (isset($_GET['error'])) { ?>
                <p class="error"><?php echo $_GET['error']; ?></p>
            <?php } ?>

            <label>ID NUMBER</label>
            <input type="text" name="idno" placeholder="ID Number" id="username" class ="login">

            <label>PASSWORD</label>
            <input type="password" name="password" placeholder="Password" class ="login">

            <button type="submit">LOG IN</button>
        </form>
    </div>
</div>
</body>
</html>