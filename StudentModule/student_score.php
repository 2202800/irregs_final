<?php
include ('header.php');

include_once('../login/connection.php');

session_start();

$pid = $_SESSION['idno'];

$query = "SELECT quiz.title, user_scores.score FROM user_scores INNER JOIN quiz ON user_scores.quiz_id=quiz.quiz_id WHERE user_scores.id = '$pid'";
$result = mysqli_query($con,$query);

$query2 = "SELECT first_name, last_name from users WHERE id = '$pid'";



$result2 = $con->query($query2);

if ($result2->num_rows > 0) {
    // output data of each row
    while($row = $result2->fetch_assoc()) {

	   echo "<p style='font-size:32px; font-family:cursive ; font-weight:bold;'> QUIZ SCORES of: " .$row["first_name"] . " " . $row["last_name"] . "</p>";

    }
} else {
       header("Location: ../login/login_form.php");
       echo "Unknown User";
}




?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name = "viewport" content="width = device-width, initial-scale = 1.0">
        <title>TEAM IRREGS | Statistics </title>
        <link rel="stylesheet" href="../styles/style.css" type = "text/css">
    </head>

    <body>
        <table class="center">
            <tr>
                <th colspan = "2"><h2 class = "table-title">Quiz Scores</h2></th>
            </tr>
            <tr>
                <th>Quiz Title</th>
                <th>Score</th>
            </tr>
            <?php while($rows = $result->fetch_assoc() ) { ?>
            <tr>
                <td><?php echo $rows['title']; ?></td>
                <td><?php echo $rows['score']; ?></td>
            </tr>
            <?php } ?>
        </table>
    </body>
</html>