<?php
    session_start();
     // for requiring connection with database and including header for quiz page
     include('../StudentModule/header.php');
     require_once('../login/connection.php'); 
              // get the quizID selected by user 
    $quizID = $_POST['quizID'];
    $_SESSION['quizID'] = $quizID;
              
    $query = "UPDATE quiz SET on_going='true' WHERE quiz_id='$quizID'"; // Query to set the on going status to TRUE
    $query_run = mysqli_query($con, $query); // Run the query to update the database, quiz table

    $questionsQuery = "SELECT * from questions WHERE quiz_id = '$quizID' "; // Query to SELECT question with a specific Quiz ID
    $quesQueryResult = mysqli_query($con, $questionsQuery);
?>

<html>
     <head>
          <title>TEAM IRREGS</title>
          <link rel="stylesheet" href="../styles/style.css" type = "text/css">
     </head>

     <body>
          <form action="checkAnswers.php" method = "POST" class = "quiz-center">
            <?php
            while($rows = mysqli_fetch_assoc($quesQueryResult)){ 
            $questionID = $rows['question_id'];
            $query = "SELECT * from choices WHERE question_id = '$questionID' "; // Query to select choices with a specific Question ID
            $queryResult = mysqli_query($con, $query);
                switch($rows['question_type']):
                        case 'Matching Type': 
            ?>
                            <div class = "item-container"> 
                                <h4><?php echo $rows['question']?></h4>
                                <select name="questionID[<?php echo $rows['question_id']?>]" id="match">
                                <?php
                                    $query = "SELECT * FROM choices JOIN questions USING (question_id) WHERE question_type = 'Matching Type' ";
                                    $queryResult = mysqli_query($con, $query); 
                                    while($rows = mysqli_fetch_assoc($queryResult)){
                                    ?>
                                        <option value="<?php echo $rows['choice_id']?>" required><?php echo $rows['choices']?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        <?php break; 
                        case 'Fill In The Blanks': ?>

                            <div class = "item-container">
                                <h4><?php echo $rows['question']?></h4>
                                <input type="text" id="fname" name="questionID[<?php echo $rows['question_id']?>]" required>
                            </div>
                            <?php break; 
                        case 'Multiple Choice': ?>
                            <div class = "item-container">
                                <h4><?php echo $rows['question']?></h4>
                                <?php 
                                while($rows = mysqli_fetch_assoc($queryResult)){
                                ?>
                                    <p><?php echo $rows['choices']?></p>
                                    <input type="radio" name="questionID[<?php echo $rows['question_id']?>]" value = "<?php echo $rows['choice_id']?>" required>
                                    <?php } ?>
                            </div>
                            <?php break; 
                                endswitch; ?>               
                <?php
                        }
                    ?>
            <div class="button-center">
                <input type="hidden" name="quizID" value="<?php echo $quizID ?>">
                <button type="submit" id="submitBTN" value="form_submitted" name="form_submitted">SUBMIT</button>
            </div>
            </form>
     </body>
</html>