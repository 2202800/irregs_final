<?php
include ('header.php');

include_once('../login/connection.php');

session_start();

$query = "SELECT * from quiz";
$result = mysqli_query($con,$query);

$pid = $_SESSION['idno'];

$query2 = "SELECT first_name, last_name from users WHERE id = '$pid'";
$result2 = $con->query($query2);


if ($result2->num_rows > 0) {
    // output data of each row
    while($row = $result2->fetch_assoc()) {

	   echo "<p class = 'welcomeParagraph'; style='font-size:32px; font-weight:bold; '> Welcome, " . $row["first_name"] . " " . $row["last_name"] .  "</p>";

    }
} else {
    header("Location: ../login/login_form.php");
    echo "Unknown User";
}

?>

<!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <meta name = "viewport" content="width = device-width, initial-scale = 1.0">
            <title>TEAM IRREGS | Technology Quiz </title>
            <link rel="stylesheet" href="../styles/style.css" type = "text/css">
        </head>

        <body>

        <table class="center">
             <tr>
                <th colspan = "3"><h2 class = "table-title">QUIZZES</h2></th>
             </tr>
            <tr>
                <th>No.</th>
                <th>Title</th> 
                <th>Status</th>
             </tr>
            <?php while($rows = $result->fetch_assoc() ) {
                $qid = $rows['quiz_id'];
                ?>
                <form action="../StudentModule/outputQuiz.php" method = "POST">
                    <?php if($rows['is_visible']=="true" ){ ?>
                    <tr>
                        <td><?php echo $rows['quiz_id']; ?></td>
                        <td><?php echo $rows['title']; ?></td>
                        <td>
                            <?php
                            // another query for session handling (quiz retaking bug)
                            $query3 = "SELECT * FROM user_scores WHERE id = '$pid' AND quiz_id = '$qid'";
                            $result3 = $con->query($query3);

                            if($rows['status']=="Accepting Responses" and $result3->num_rows == 0){ ?>
                                <button type="submit" id="takequiz" name="quizID" value="<?php echo$rows['quiz_id']?>">Take Quiz</button></a>
                            <?php } else{
                                if ($result3->num_rows > 0) {
                                    echo "You have already taken this quiz.";
                                } else {
                                    echo "Unavailable";
                                }
                            } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </form>
            <?php } ?>
        </table>

        </body>
    </html>