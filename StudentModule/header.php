<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name = "viewport" content="width = device-width, initial-scale = 1.0">
    <link rel="stylesheet" href="../styles/style.css" type = "text/css">
</head>

    <body>
        <div class="header">
            <a href="../StudentModule/main.php" id="logo"><img src="../lib/images/WHITELOGO.png" alt="Team IRREGS" height="50px" width="150px"></a>
            <div class="header-right">
                <a class="active" href="../StudentModule/main.php">Home</a>
                <a href="../StudentModule/student_score.php">Scores</a>
                <a href="../login/logout.php">Logout</a>
            </div>
        </div>
    </body>

</html>