<?php 
    session_start();
    require('../login/connection.php');
    include('header.php');

    if(isset($_POST['form_submitted'])){
        $query = "UPDATE quiz SET on_going='false' WHERE quiz_id='{$_SESSION['quizID']}'"; // Query to set the on going status to FALSE
        $query_run = mysqli_query($con, $query); // Run the query to update the database, quiz table    
        $inputArray = $_POST['questionID'];
        $value = current($inputArray);
        $score = 0;
        $Question_query = "SELECT * from questions WHERE quiz_id = '{$_SESSION['quizID']}'";
        $question_run = mysqli_query($con, $Question_query);
?>



<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name = "viewport" content="width = device-width, initial-scale = 1.0">
        <title>TEAM IRREGS</title>
        <link rel="stylesheet" href="../styles/style.css" type = "text/css">
    </head>

    <body>
        <div class="quiz-center">
            
            <?php while($rows = mysqli_fetch_assoc($question_run)){ ?>
                <div class="item-container">
                <?php         
                echo "<p>" .$rows['question']. "</p>";       
                $questionID = $rows['question_id'];
                $CorrectAns_Query = "SELECT * from checking_table WHERE question_id = '$questionID'";
                $correctAns_run = mysqli_query($con, $CorrectAns_Query);

                while($result = mysqli_fetch_assoc($correctAns_run)){
                echo "<p> Correct Answer: " .$result['correctAnswer']. "</p>";
                $correctAns = $result['correctAnswer'];

                    $FillInTheblanks_Query = "SELECT question_type FROM questions WHERE question_id ='$questionID'";
                    $FITB_qrun = mysqli_query($con, $FillInTheblanks_Query);
                    while($FITB_type = mysqli_fetch_assoc($FITB_qrun)){
                        $questionType = $FITB_type['question_type'];
                    }
                    if($questionType == "Fill In The Blanks"){
                        $userAnswer = $value;
                        echo "<p> Your Answer: " .$userAnswer. "</p>";
                        if($correctAns == $value){
                            $score++;
                        }
                        $value = next($inputArray);
                    }else{
                        $choice_Query = "SELECT * FROM choices WHERE choice_id = '$value'";
                        $choice_run = mysqli_query($con, $choice_Query);
                    
                            while($choices = mysqli_fetch_assoc($choice_run)){
                                $userAnswer = $choices['choices'];
                                echo "<p> Your Answer: " .$userAnswer. "</p>";
                                if($correctAns == $userAnswer){
                                    $score++;
                                }
                                $value = next($inputArray);
                            }
                    }
                     $insertQuery = "INSERT INTO user_choices VALUES ('{$_SESSION['idno']}','$questionID','$userAnswer')";
                     $query_run = mysqli_query($con, $insertQuery);

                }?>
                </div>
                <?php
            }
           
            echo "<p> TOTAL SCORE: " .$score. "</p>";

            // to insert student scores in the database
            $quizID = $_SESSION['quizID'];
            $insertQuery = "INSERT INTO user_scores VALUES ('{$_SESSION['idno']}','$score','$quizID')";
            $query_run = mysqli_query($con, $insertQuery);

            }
            ?>

           
        </div>
    </body>
</html>